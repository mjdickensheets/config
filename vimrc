""MOVEMENT""
nmap <S-W> gegew
nmap <S-E> ge
"SYNTAX""
syntax enable

set tabstop=4 "number of visual spaces for tab
set softtabstop=4 "number of spaces in tab when editing
set shiftwidth=4 "number of spaces added after carrage return
set expandtab "tabs are spaces
inoremap <S-Tab> <C-V><Tab>

""SPELL CHECK""
set spelllang=en

""UI""
set number relativenumber "show line number
set showcmd "show command in bottom bar
set cursorline "highlight current line
filetype indent on "load filetype-specific indent files
set wildmenu "visual autocomplete for command menu
set lazyredraw "only redraw when necessary
set showmatch "highlight matching [{()}]

""SEARCHES""
set incsearch "search as text is entered
set hlsearch "highlight results

""FOLDING""
set foldenable "enable folding
set foldlevelstart=10 "open most folds by default
set foldnestmax=10 "max fold nesting
set foldmethod=indent "fold based on indent level

""TABS AND SPLITS""
noremap <F7> gT 
noremap <F8> gt
map <C-J> <C-W>j
map <C-K> <C-W>k
nmap <c-h> <c-w>h
nmap <c-l> <c-w>l

""BACKUP, UNDO, SWP""
set backup
set backupdir=~/.vim/.backup//
set writebackup
set undodir=~/.vim/.undo//
set directory=~/.vim/.swp//

""PATHOGEN""
execute pathogen#infect()


""Colors""
let g:seoul256_background = 236
colo seoul256

"LIGHTLINE CONFIG"
set laststatus=2
set noshowmode
let g:lightline = {
    \ 'colorscheme': 'seoul256',
    \ }

"NERDTREE CONFIG"
nmap <C-O> :NERDTree<CR>
